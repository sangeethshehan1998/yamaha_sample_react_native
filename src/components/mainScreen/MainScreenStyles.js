import {
    PixelRatio,
    StyleSheet,
} from 'react-native';

const styles = StyleSheet.create({

    TitleStyle: {
        fontWeight:'bold',
        fontSize:18,
        left:10,
        top:5
    },
    PriceStyle:{
        fontWeight:'bold',
        fontSize:25,
        left:10,
        top:10
    },
    ratingContainerStyles:{
        flexDirection: 'row',
    },
    ratingTextStyle:{
        fontSize:18,
        left:10,
        top:5,
        color:'gray'
    },
    gridView: {
        marginTop: 20,
     
      },
      itemContainer: {
        justifyContent: 'flex-end',
        borderRadius: 10,
        elevation: 2,
        width:120,
        padding: 10,
        height: 120,
        margin:10,
      },
      FlatListContainerStyles:{
        justifyContent:'center',
        alignContent:'center',
        alignItems:'center'
      },
      itemName: {
        fontSize: 12,
        color: 'black',
        fontWeight: '600',
      },
      iconlImage: {
        width: 45,
        height: 40
      },
      itemCode: {
        fontWeight: 'bold',
        fontSize: 18,
        color: 'black',
      },
});


export default styles;
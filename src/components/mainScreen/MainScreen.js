
import React, { Component } from 'react';
import {
    View, Text, FlatList, Image
} from 'react-native';
import { SliderBox } from 'react-native-image-slider-box';
import { Rating, AirbnbRating } from 'react-native-ratings';
import styles from './MainScreenStyles'
import Colors from '../../resources/Colors';


export default class MainScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            images: [
                'https://i.ibb.co/BcCp82X/Untitled-1.jpg',
                'https://i.ibb.co/BcCp82X/Untitled-1.jpg',
                'https://i.ibb.co/BcCp82X/Untitled-1.jpg',
                'https://i.ibb.co/BcCp82X/Untitled-1.jpg'
            ],
            rating: '',
            items: [
                {
                    title: "Engine Capacity",
                    sub: "149.0 CC",
                    name: require("../../assets/Engine.png"),

                },
                {
                    title: "Max Power",
                    sub: "8000 rpm",
                    name: require("../../assets/MaxPower.png"),

                },
                {
                    title: "Mileage",
                    sub: "149.0 CC",
                    name: require("../../assets/Mileage.png"),

                },
                {
                    title: "Transmission",
                    sub: "149.0 CC",
                    name: require("../../assets/Transmission.png"),

                },

            ]
        }
    }

    //counting rating
    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }


    render() {
        return (
            <View style={{ flex: 1, }}>
                <SliderBox
                    dotColor="#1092e3"
                    inactiveDotColor="#90A4AE"
                    sliderBoxHeight={400}
                    images={this.state.images} />

                <View>
                    <Text style={styles.TitleStyle}>Yamaha FZ V3.0 F1</Text>
                    <View style={styles.ratingContainerStyles}>
                        <Text style={styles.ratingTextStyle}>(4/5)1240 ratings     </Text>
                        <Rating
                            imageSize={20}
                            onFinishRating={this.ratingCompleted}
                            style={{ paddingVertical: 10 }}
                        />
                    </View>
                    <Text style={styles.PriceStyle}>Rs 400,000.00</Text>

                    <FlatList
                        itemDimension={130}
                        data={this.state.items}
                        horizontal={true}
                        style={styles.gridView}
                        renderItem={({ item, index }) => (
                            <View style={[styles.itemContainer, { backgroundColor: Colors.DARK_GRAY }]}>
                                <View style={styles.FlatListContainerStyles}>
                                    <Image
                                        name={item.name}
                                        style={styles.iconlImage}
                                        source={item.name}
                                    />
                                    <Text style={styles.itemName}>{item.title}</Text>
                                    <Text style={styles.itemCode}>{item.sub}</Text>
                                </View>
                            </View>
                        )}
                    />
                    <Text style={styles.TitleStyle}>Specifications</Text>
                </View>

            </View>
        )
    }
}


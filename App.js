/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react';
import { StatusBar, View, } from 'react-native'
import { createAppContainer } from 'react-navigation';
import { createStackNavigator, Header } from 'react-navigation-stack';


//Screens
import MainScreen from './src/components/mainScreen/MainScreen';



const RootStack = createStackNavigator({
  MainScreen: {
    screen: MainScreen,
    navigationOptions: { header: null }
  },
},

  {
    initialRouteName: 'MainScreen'
  },
  {
    headerMode: 'screen'
  },
);

const AppContainer = createAppContainer(RootStack);

export default class App extends Component {

  render() {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar barStyle="dark-content" hidden={false} backgroundColor="white" />
        <AppContainer />
      </View>
    );
  }
}




